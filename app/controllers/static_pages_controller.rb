class StaticPagesController < ApplicationController
  def home
   @user = current_user
  end

  def help
  end

  def about
  end
  
  def academics
  end
 
  def analytics
  end

  def timetable
  end

  def calendar
   @month = (params[:month] || Time.zone.now.month).to_i
   @year = (params[:year] || Time.zone.now.year).to_i
   @shown_month = Date.civil(@year, @month)
   @event_strips = Event.event_strips_for_month(@shown_month)
 end
end
