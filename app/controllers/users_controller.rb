class UsersController < ApplicationController
  before_action :signed_in_user, only: [:edit, :update]
   before_action :correct_user,   only: [:edit, :update]
  def new
    @user = User.new
  end
  def show
    @user = User.find(params[:id])
  end
  def create
    @user = User.new(user_params)
    if @user.save
      # Handle a successful save.
      sign_in @user
      redirect_to root_path
      flash[:success] = "Welcome to the Sample App!"
    else
      render 'new'
    end
  end

  def index
    redirect_to root_path
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to root_path
    else
      render 'edit'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    def signed_in_user
      redirect_to signin_url, notice: "Please sign in." unless signed_in?
    end

   
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

   def current_user?(user)
    user == current_user
   end
end
