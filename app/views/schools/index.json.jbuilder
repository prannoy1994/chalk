json.array!(@schools) do |school|
  json.extract! school, :school_id, :school_name, :school_address, :school_phone, :school_head_id
  json.url school_url(school, format: :json)
end
