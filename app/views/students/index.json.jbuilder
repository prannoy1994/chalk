json.array!(@students) do |student|
  json.extract! student, :student_school_id, :student_standard_id, :student_id, :student_name, :student_address, :student_city, :student_state, :student_phone
  json.url student_url(student, format: :json)
end
