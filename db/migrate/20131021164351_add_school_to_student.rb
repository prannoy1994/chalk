class AddSchoolToStudent < ActiveRecord::Migration
  def change
    add_column :students, :school_id, :string
  end
end
