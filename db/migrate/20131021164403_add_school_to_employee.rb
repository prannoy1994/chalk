class AddSchoolToEmployee < ActiveRecord::Migration
  def change
    add_column :employees, :school_id, :string
  end
end
