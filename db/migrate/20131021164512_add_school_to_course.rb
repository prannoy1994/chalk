class AddSchoolToCourse < ActiveRecord::Migration
  def change
    add_column :courses, :school_id, :string
  end
end
