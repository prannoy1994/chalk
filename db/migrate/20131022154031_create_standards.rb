class CreateStandards < ActiveRecord::Migration
  def change
    create_table :standards do |t|
      t.string :standard_school_id
      t.string :standard_id
      t.string :standard_classteacher_id

      t.timestamps
    end
  end
end
