class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :student_school_id
      t.string :student_standard_id
      t.string :student_id
      t.string :student_name
      t.text :student_address
      t.string :student_city
      t.string :student_state
      t.string :student_phone

      t.timestamps
    end
  end
end
