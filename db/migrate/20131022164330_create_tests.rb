class CreateTests < ActiveRecord::Migration
  def change
    create_table :tests do |t|
      t.string :school_id
      t.string :subject_id
      t.string :test_id
      t.string :test_name
      t.decimal :test_total_marks

      t.timestamps
    end
  end
end
