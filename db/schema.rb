# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131023115947) do

  create_table "course_enrolments", force: true do |t|
    t.string   "course_id"
    t.string   "student_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courses", force: true do |t|
    t.string   "course_id"
    t.string   "course_name"
    t.string   "teacher_id"
    t.string   "teacher_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", force: true do |t|
    t.string   "employee_id"
    t.string   "employee_first_name"
    t.string   "employee_last_name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "phone_number"
    t.string   "department_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "school_id"
  end

  create_table "events", force: true do |t|
    t.string   "name"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "marks", force: true do |t|
    t.string   "school_id"
    t.string   "student_id"
    t.string   "standard_id"
    t.string   "subject_id"
    t.string   "test_id"
    t.decimal  "marks",       precision: 10, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "schools", force: true do |t|
    t.string   "school_id"
    t.string   "school_name"
    t.text     "school_address"
    t.string   "school_phone"
    t.string   "school_head_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "standards", force: true do |t|
    t.string   "standard_school_id"
    t.string   "standard_id"
    t.string   "standard_classteacher_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "students", force: true do |t|
    t.string   "student_school_id"
    t.string   "student_standard_id"
    t.string   "student_id"
    t.string   "student_name"
    t.text     "student_address"
    t.string   "student_city"
    t.string   "student_state"
    t.string   "student_phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subjects", force: true do |t|
    t.string   "school_id"
    t.string   "subject_id"
    t.string   "subject_standard_id"
    t.string   "subject_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teacher_subject_maps", force: true do |t|
    t.string   "school_id"
    t.string   "teacher_id"
    t.string   "standard_id"
    t.string   "subject_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teachers", force: true do |t|
    t.string   "teacher_school_id"
    t.string   "teacher_id"
    t.string   "teacher_name"
    t.text     "teacher_address"
    t.string   "teacher_city"
    t.string   "teacher_state"
    t.string   "teacher_phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tests", force: true do |t|
    t.string   "school_id"
    t.string   "subject_id"
    t.string   "test_id"
    t.string   "test_name"
    t.decimal  "test_total_marks", precision: 10, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.string   "remember_token"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["remember_token"], name: "index_users_on_remember_token", using: :btree

end
