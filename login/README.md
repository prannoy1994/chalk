#Code repository for chalkspace.com

1. Uses designmodo FlatUI
2. Uses mysql (username=root, password=zevon, change in config/database.yml)

##Contributor guidelines
Create your own branch and work. Send me a pull request or a patch and *I'll do the merges*.

All commit messages to be written in **PRESENT TENSE**. Example: "Add ninja skill." and not "Added ninja skill."

More will come.

##Database Structure
###scaffolds:
(replace - with _ for actual field names)

1. School
	school-id | school-name | school-address | school-phone | school-head-id


2. Teacher
	teacher-school-id | teacher-id | teacher-name | teacher-address | teacher-city | teacher-state | teacher-phone


3. Student
	student-school-id | student-standard-id | student-id | student-name | student-address | student-city | student-state | student-phone
	

4. Standard
	standard-school-id | standard-id | standard-classteacher-id


5. Subject
	school-id | subject-id | subject-standard-id | subject-name


6. Test
	school-id | subject-id | test-id | test-name | test-total-marks

###models:
1. teacher-subject-maps		(mind the 's')
	school-id | teacher-id | standard-id | subject-id

