json.array!(@marks) do |mark|
  json.extract! mark, :school_id, :student_id, :standard_id, :subject_id, :test_id, :marks
  json.url mark_url(mark, format: :json)
end
