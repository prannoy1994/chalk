json.array!(@standards) do |standard|
  json.extract! standard, :standard_school_id, :standard_id, :standard_classteacher_id
  json.url standard_url(standard, format: :json)
end
