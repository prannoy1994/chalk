json.array!(@subjects) do |subject|
  json.extract! subject, :school_id, :subject_id, :subject_standard_id, :subject_name
  json.url subject_url(subject, format: :json)
end
