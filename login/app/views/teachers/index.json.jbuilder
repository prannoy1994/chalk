json.array!(@teachers) do |teacher|
  json.extract! teacher, :teacher_school_id, :teacher_id, :teacher_name, :teacher_address, :teacher_city, :teacher_state, :teacher_phone
  json.url teacher_url(teacher, format: :json)
end
