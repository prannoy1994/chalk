json.array!(@tests) do |test|
  json.extract! test, :school_id, :subject_id, :test_id, :test_name, :test_total_marks
  json.url test_url(test, format: :json)
end
