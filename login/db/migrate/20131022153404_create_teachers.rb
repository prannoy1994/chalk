class CreateTeachers < ActiveRecord::Migration
  def change
    create_table :teachers do |t|
      t.string :teacher_school_id
      t.string :teacher_id
      t.string :teacher_name
      t.text :teacher_address
      t.string :teacher_city
      t.string :teacher_state
      t.string :teacher_phone

      t.timestamps
    end
  end
end
