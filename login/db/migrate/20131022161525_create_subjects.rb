class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :school_id
      t.string :subject_id
      t.string :subject_standard_id
      t.string :subject_name

      t.timestamps
    end
  end
end
