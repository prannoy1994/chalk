class CreateTeacherSubjectMaps < ActiveRecord::Migration
  def change
    create_table :teacher_subject_maps do |t|
      t.string :school_id
      t.string :teacher_id
      t.string :standard_id
      t.string :subject_id

      t.timestamps
    end
  end
end
