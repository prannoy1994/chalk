class CreateMarks < ActiveRecord::Migration
  def change
    create_table :marks do |t|
      t.string :school_id
      t.string :student_id
      t.string :standard_id
      t.string :subject_id
      t.string :test_id
      t.decimal :marks

      t.timestamps
    end
  end
end
