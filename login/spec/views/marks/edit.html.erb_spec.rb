require 'spec_helper'

describe "marks/edit" do
  before(:each) do
    @mark = assign(:mark, stub_model(Mark,
      :school_id => "MyString",
      :student_id => "MyString",
      :standard_id => "MyString",
      :subject_id => "MyString",
      :test_id => "MyString",
      :marks => "9.99"
    ))
  end

  it "renders the edit mark form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", mark_path(@mark), "post" do
      assert_select "input#mark_school_id[name=?]", "mark[school_id]"
      assert_select "input#mark_student_id[name=?]", "mark[student_id]"
      assert_select "input#mark_standard_id[name=?]", "mark[standard_id]"
      assert_select "input#mark_subject_id[name=?]", "mark[subject_id]"
      assert_select "input#mark_test_id[name=?]", "mark[test_id]"
      assert_select "input#mark_marks[name=?]", "mark[marks]"
    end
  end
end
