require 'spec_helper'

describe "schools/index" do
  before(:each) do
    assign(:schools, [
      stub_model(School,
        :school_id => "School",
        :school_name => "School Name",
        :school_address => "MyText",
        :school_phone => "School Phone",
        :school_head_id => "School Head"
      ),
      stub_model(School,
        :school_id => "School",
        :school_name => "School Name",
        :school_address => "MyText",
        :school_phone => "School Phone",
        :school_head_id => "School Head"
      )
    ])
  end

  it "renders a list of schools" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "School".to_s, :count => 2
    assert_select "tr>td", :text => "School Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "School Phone".to_s, :count => 2
    assert_select "tr>td", :text => "School Head".to_s, :count => 2
  end
end
