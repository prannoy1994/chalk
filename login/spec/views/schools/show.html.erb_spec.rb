require 'spec_helper'

describe "schools/show" do
  before(:each) do
    @school = assign(:school, stub_model(School,
      :school_id => "School",
      :school_name => "School Name",
      :school_address => "MyText",
      :school_phone => "School Phone",
      :school_head_id => "School Head"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/School/)
    rendered.should match(/School Name/)
    rendered.should match(/MyText/)
    rendered.should match(/School Phone/)
    rendered.should match(/School Head/)
  end
end
