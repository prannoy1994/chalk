require 'spec_helper'

describe "standards/index" do
  before(:each) do
    assign(:standards, [
      stub_model(Standard,
        :standard_school_id => "Standard School",
        :standard_id => "Standard",
        :standard_classteacher_id => "Standard Classteacher"
      ),
      stub_model(Standard,
        :standard_school_id => "Standard School",
        :standard_id => "Standard",
        :standard_classteacher_id => "Standard Classteacher"
      )
    ])
  end

  it "renders a list of standards" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Standard School".to_s, :count => 2
    assert_select "tr>td", :text => "Standard".to_s, :count => 2
    assert_select "tr>td", :text => "Standard Classteacher".to_s, :count => 2
  end
end
