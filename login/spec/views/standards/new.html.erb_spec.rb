require 'spec_helper'

describe "standards/new" do
  before(:each) do
    assign(:standard, stub_model(Standard,
      :standard_school_id => "MyString",
      :standard_id => "MyString",
      :standard_classteacher_id => "MyString"
    ).as_new_record)
  end

  it "renders new standard form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", standards_path, "post" do
      assert_select "input#standard_standard_school_id[name=?]", "standard[standard_school_id]"
      assert_select "input#standard_standard_id[name=?]", "standard[standard_id]"
      assert_select "input#standard_standard_classteacher_id[name=?]", "standard[standard_classteacher_id]"
    end
  end
end
