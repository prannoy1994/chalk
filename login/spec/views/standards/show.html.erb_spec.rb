require 'spec_helper'

describe "standards/show" do
  before(:each) do
    @standard = assign(:standard, stub_model(Standard,
      :standard_school_id => "Standard School",
      :standard_id => "Standard",
      :standard_classteacher_id => "Standard Classteacher"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Standard School/)
    rendered.should match(/Standard/)
    rendered.should match(/Standard Classteacher/)
  end
end
