require 'spec_helper'

describe "students/index" do
  before(:each) do
    assign(:students, [
      stub_model(Student,
        :student_school_id => "Student School",
        :student_standard_id => "Student Standard",
        :student_id => "Student",
        :student_name => "Student Name",
        :student_address => "MyText",
        :student_city => "Student City",
        :student_state => "Student State",
        :student_phone => "Student Phone"
      ),
      stub_model(Student,
        :student_school_id => "Student School",
        :student_standard_id => "Student Standard",
        :student_id => "Student",
        :student_name => "Student Name",
        :student_address => "MyText",
        :student_city => "Student City",
        :student_state => "Student State",
        :student_phone => "Student Phone"
      )
    ])
  end

  it "renders a list of students" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Student School".to_s, :count => 2
    assert_select "tr>td", :text => "Student Standard".to_s, :count => 2
    assert_select "tr>td", :text => "Student".to_s, :count => 2
    assert_select "tr>td", :text => "Student Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Student City".to_s, :count => 2
    assert_select "tr>td", :text => "Student State".to_s, :count => 2
    assert_select "tr>td", :text => "Student Phone".to_s, :count => 2
  end
end
