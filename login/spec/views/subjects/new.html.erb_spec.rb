require 'spec_helper'

describe "subjects/new" do
  before(:each) do
    assign(:subject, stub_model(Subject,
      :school_id => "MyString",
      :subject_id => "MyString",
      :subject_standard_id => "MyString",
      :subject_name => "MyString"
    ).as_new_record)
  end

  it "renders new subject form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", subjects_path, "post" do
      assert_select "input#subject_school_id[name=?]", "subject[school_id]"
      assert_select "input#subject_subject_id[name=?]", "subject[subject_id]"
      assert_select "input#subject_subject_standard_id[name=?]", "subject[subject_standard_id]"
      assert_select "input#subject_subject_name[name=?]", "subject[subject_name]"
    end
  end
end
