require 'spec_helper'

describe "subjects/show" do
  before(:each) do
    @subject = assign(:subject, stub_model(Subject,
      :school_id => "School",
      :subject_id => "Subject",
      :subject_standard_id => "Subject Standard",
      :subject_name => "Subject Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/School/)
    rendered.should match(/Subject/)
    rendered.should match(/Subject Standard/)
    rendered.should match(/Subject Name/)
  end
end
