require 'spec_helper'

describe "teachers/edit" do
  before(:each) do
    @teacher = assign(:teacher, stub_model(Teacher,
      :teacher_school_id => "MyString",
      :teacher_id => "MyString",
      :teacher_name => "MyString",
      :teacher_address => "MyText",
      :teacher_city => "MyString",
      :teacher_state => "MyString",
      :teacher_phone => "MyString"
    ))
  end

  it "renders the edit teacher form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", teacher_path(@teacher), "post" do
      assert_select "input#teacher_teacher_school_id[name=?]", "teacher[teacher_school_id]"
      assert_select "input#teacher_teacher_id[name=?]", "teacher[teacher_id]"
      assert_select "input#teacher_teacher_name[name=?]", "teacher[teacher_name]"
      assert_select "textarea#teacher_teacher_address[name=?]", "teacher[teacher_address]"
      assert_select "input#teacher_teacher_city[name=?]", "teacher[teacher_city]"
      assert_select "input#teacher_teacher_state[name=?]", "teacher[teacher_state]"
      assert_select "input#teacher_teacher_phone[name=?]", "teacher[teacher_phone]"
    end
  end
end
