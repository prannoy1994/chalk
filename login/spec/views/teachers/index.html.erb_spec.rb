require 'spec_helper'

describe "teachers/index" do
  before(:each) do
    assign(:teachers, [
      stub_model(Teacher,
        :teacher_school_id => "Teacher School",
        :teacher_id => "Teacher",
        :teacher_name => "Teacher Name",
        :teacher_address => "MyText",
        :teacher_city => "Teacher City",
        :teacher_state => "Teacher State",
        :teacher_phone => "Teacher Phone"
      ),
      stub_model(Teacher,
        :teacher_school_id => "Teacher School",
        :teacher_id => "Teacher",
        :teacher_name => "Teacher Name",
        :teacher_address => "MyText",
        :teacher_city => "Teacher City",
        :teacher_state => "Teacher State",
        :teacher_phone => "Teacher Phone"
      )
    ])
  end

  it "renders a list of teachers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Teacher School".to_s, :count => 2
    assert_select "tr>td", :text => "Teacher".to_s, :count => 2
    assert_select "tr>td", :text => "Teacher Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Teacher City".to_s, :count => 2
    assert_select "tr>td", :text => "Teacher State".to_s, :count => 2
    assert_select "tr>td", :text => "Teacher Phone".to_s, :count => 2
  end
end
