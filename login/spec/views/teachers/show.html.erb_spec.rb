require 'spec_helper'

describe "teachers/show" do
  before(:each) do
    @teacher = assign(:teacher, stub_model(Teacher,
      :teacher_school_id => "Teacher School",
      :teacher_id => "Teacher",
      :teacher_name => "Teacher Name",
      :teacher_address => "MyText",
      :teacher_city => "Teacher City",
      :teacher_state => "Teacher State",
      :teacher_phone => "Teacher Phone"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Teacher School/)
    rendered.should match(/Teacher/)
    rendered.should match(/Teacher Name/)
    rendered.should match(/MyText/)
    rendered.should match(/Teacher City/)
    rendered.should match(/Teacher State/)
    rendered.should match(/Teacher Phone/)
  end
end
