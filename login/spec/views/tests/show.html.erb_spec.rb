require 'spec_helper'

describe "tests/show" do
  before(:each) do
    @test = assign(:test, stub_model(Test,
      :school_id => "School",
      :subject_id => "Subject",
      :test_id => "Test",
      :test_name => "Test Name",
      :test_total_marks => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/School/)
    rendered.should match(/Subject/)
    rendered.should match(/Test/)
    rendered.should match(/Test Name/)
    rendered.should match(/9.99/)
  end
end
