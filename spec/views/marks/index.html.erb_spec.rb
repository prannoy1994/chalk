require 'spec_helper'

describe "marks/index" do
  before(:each) do
    assign(:marks, [
      stub_model(Mark,
        :school_id => "School",
        :student_id => "Student",
        :standard_id => "Standard",
        :subject_id => "Subject",
        :test_id => "Test",
        :marks => "9.99"
      ),
      stub_model(Mark,
        :school_id => "School",
        :student_id => "Student",
        :standard_id => "Standard",
        :subject_id => "Subject",
        :test_id => "Test",
        :marks => "9.99"
      )
    ])
  end

  it "renders a list of marks" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "School".to_s, :count => 2
    assert_select "tr>td", :text => "Student".to_s, :count => 2
    assert_select "tr>td", :text => "Standard".to_s, :count => 2
    assert_select "tr>td", :text => "Subject".to_s, :count => 2
    assert_select "tr>td", :text => "Test".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
