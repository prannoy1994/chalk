require 'spec_helper'

describe "marks/show" do
  before(:each) do
    @mark = assign(:mark, stub_model(Mark,
      :school_id => "School",
      :student_id => "Student",
      :standard_id => "Standard",
      :subject_id => "Subject",
      :test_id => "Test",
      :marks => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/School/)
    rendered.should match(/Student/)
    rendered.should match(/Standard/)
    rendered.should match(/Subject/)
    rendered.should match(/Test/)
    rendered.should match(/9.99/)
  end
end
