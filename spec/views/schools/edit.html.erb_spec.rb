require 'spec_helper'

describe "schools/edit" do
  before(:each) do
    @school = assign(:school, stub_model(School,
      :school_id => "MyString",
      :school_name => "MyString",
      :school_address => "MyText",
      :school_phone => "MyString",
      :school_head_id => "MyString"
    ))
  end

  it "renders the edit school form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", school_path(@school), "post" do
      assert_select "input#school_school_id[name=?]", "school[school_id]"
      assert_select "input#school_school_name[name=?]", "school[school_name]"
      assert_select "textarea#school_school_address[name=?]", "school[school_address]"
      assert_select "input#school_school_phone[name=?]", "school[school_phone]"
      assert_select "input#school_school_head_id[name=?]", "school[school_head_id]"
    end
  end
end
