require 'spec_helper'

describe "schools/new" do
  before(:each) do
    assign(:school, stub_model(School,
      :school_id => "MyString",
      :school_name => "MyString",
      :school_address => "MyText",
      :school_phone => "MyString",
      :school_head_id => "MyString"
    ).as_new_record)
  end

  it "renders new school form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", schools_path, "post" do
      assert_select "input#school_school_id[name=?]", "school[school_id]"
      assert_select "input#school_school_name[name=?]", "school[school_name]"
      assert_select "textarea#school_school_address[name=?]", "school[school_address]"
      assert_select "input#school_school_phone[name=?]", "school[school_phone]"
      assert_select "input#school_school_head_id[name=?]", "school[school_head_id]"
    end
  end
end
