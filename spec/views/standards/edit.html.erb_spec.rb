require 'spec_helper'

describe "standards/edit" do
  before(:each) do
    @standard = assign(:standard, stub_model(Standard,
      :standard_school_id => "MyString",
      :standard_id => "MyString",
      :standard_classteacher_id => "MyString"
    ))
  end

  it "renders the edit standard form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", standard_path(@standard), "post" do
      assert_select "input#standard_standard_school_id[name=?]", "standard[standard_school_id]"
      assert_select "input#standard_standard_id[name=?]", "standard[standard_id]"
      assert_select "input#standard_standard_classteacher_id[name=?]", "standard[standard_classteacher_id]"
    end
  end
end
