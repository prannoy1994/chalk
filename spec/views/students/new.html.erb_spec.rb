require 'spec_helper'

describe "students/new" do
  before(:each) do
    assign(:student, stub_model(Student,
      :student_school_id => "MyString",
      :student_standard_id => "MyString",
      :student_id => "MyString",
      :student_name => "MyString",
      :student_address => "MyText",
      :student_city => "MyString",
      :student_state => "MyString",
      :student_phone => "MyString"
    ).as_new_record)
  end

  it "renders new student form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", students_path, "post" do
      assert_select "input#student_student_school_id[name=?]", "student[student_school_id]"
      assert_select "input#student_student_standard_id[name=?]", "student[student_standard_id]"
      assert_select "input#student_student_id[name=?]", "student[student_id]"
      assert_select "input#student_student_name[name=?]", "student[student_name]"
      assert_select "textarea#student_student_address[name=?]", "student[student_address]"
      assert_select "input#student_student_city[name=?]", "student[student_city]"
      assert_select "input#student_student_state[name=?]", "student[student_state]"
      assert_select "input#student_student_phone[name=?]", "student[student_phone]"
    end
  end
end
