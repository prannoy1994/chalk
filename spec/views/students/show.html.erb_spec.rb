require 'spec_helper'

describe "students/show" do
  before(:each) do
    @student = assign(:student, stub_model(Student,
      :student_school_id => "Student School",
      :student_standard_id => "Student Standard",
      :student_id => "Student",
      :student_name => "Student Name",
      :student_address => "MyText",
      :student_city => "Student City",
      :student_state => "Student State",
      :student_phone => "Student Phone"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Student School/)
    rendered.should match(/Student Standard/)
    rendered.should match(/Student/)
    rendered.should match(/Student Name/)
    rendered.should match(/MyText/)
    rendered.should match(/Student City/)
    rendered.should match(/Student State/)
    rendered.should match(/Student Phone/)
  end
end
