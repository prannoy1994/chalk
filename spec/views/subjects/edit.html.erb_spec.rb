require 'spec_helper'

describe "subjects/edit" do
  before(:each) do
    @subject = assign(:subject, stub_model(Subject,
      :school_id => "MyString",
      :subject_id => "MyString",
      :subject_standard_id => "MyString",
      :subject_name => "MyString"
    ))
  end

  it "renders the edit subject form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", subject_path(@subject), "post" do
      assert_select "input#subject_school_id[name=?]", "subject[school_id]"
      assert_select "input#subject_subject_id[name=?]", "subject[subject_id]"
      assert_select "input#subject_subject_standard_id[name=?]", "subject[subject_standard_id]"
      assert_select "input#subject_subject_name[name=?]", "subject[subject_name]"
    end
  end
end
