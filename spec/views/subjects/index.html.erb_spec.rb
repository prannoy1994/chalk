require 'spec_helper'

describe "subjects/index" do
  before(:each) do
    assign(:subjects, [
      stub_model(Subject,
        :school_id => "School",
        :subject_id => "Subject",
        :subject_standard_id => "Subject Standard",
        :subject_name => "Subject Name"
      ),
      stub_model(Subject,
        :school_id => "School",
        :subject_id => "Subject",
        :subject_standard_id => "Subject Standard",
        :subject_name => "Subject Name"
      )
    ])
  end

  it "renders a list of subjects" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "School".to_s, :count => 2
    assert_select "tr>td", :text => "Subject".to_s, :count => 2
    assert_select "tr>td", :text => "Subject Standard".to_s, :count => 2
    assert_select "tr>td", :text => "Subject Name".to_s, :count => 2
  end
end
