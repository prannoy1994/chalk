require 'spec_helper'

describe "tests/edit" do
  before(:each) do
    @test = assign(:test, stub_model(Test,
      :school_id => "MyString",
      :subject_id => "MyString",
      :test_id => "MyString",
      :test_name => "MyString",
      :test_total_marks => "9.99"
    ))
  end

  it "renders the edit test form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", test_path(@test), "post" do
      assert_select "input#test_school_id[name=?]", "test[school_id]"
      assert_select "input#test_subject_id[name=?]", "test[subject_id]"
      assert_select "input#test_test_id[name=?]", "test[test_id]"
      assert_select "input#test_test_name[name=?]", "test[test_name]"
      assert_select "input#test_test_total_marks[name=?]", "test[test_total_marks]"
    end
  end
end
