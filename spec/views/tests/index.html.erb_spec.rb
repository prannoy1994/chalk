require 'spec_helper'

describe "tests/index" do
  before(:each) do
    assign(:tests, [
      stub_model(Test,
        :school_id => "School",
        :subject_id => "Subject",
        :test_id => "Test",
        :test_name => "Test Name",
        :test_total_marks => "9.99"
      ),
      stub_model(Test,
        :school_id => "School",
        :subject_id => "Subject",
        :test_id => "Test",
        :test_name => "Test Name",
        :test_total_marks => "9.99"
      )
    ])
  end

  it "renders a list of tests" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "School".to_s, :count => 2
    assert_select "tr>td", :text => "Subject".to_s, :count => 2
    assert_select "tr>td", :text => "Test".to_s, :count => 2
    assert_select "tr>td", :text => "Test Name".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
